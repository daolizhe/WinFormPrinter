﻿using BarcodeLib;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace PrinterTool
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        #region 小标签打印需要的参数
        string barcode_sub01 = "";
        #endregion
        /// <summary>
        /// 生成条形码
        /// </summary>
        /// <param name="content">内容</param>
        /// <returns></returns>
        public static Image GenerateBarCodeBitmap_sub02(string content, int width = 200, int height = 40)
        {
            using (var barcode = new Barcode()
            {
                IncludeLabel = false,
                Alignment = AlignmentPositions.CENTER,
                Width = width,
                Height = height,
                RotateFlipType = RotateFlipType.RotateNoneFlipNone,
                BackColor = Color.White,
                ForeColor = Color.Black,
                LabelPosition = BarcodeLib.LabelPositions.BOTTOMCENTER,
            })
            {
                return barcode.Encode(TYPE.CODE128B, content);
            }
        }
        private void printDocument5_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            printDocument5.DefaultPageSettings.Margins.Top = 0;
            printDocument5.DefaultPageSettings.Margins.Bottom = 0;
            printDocument5.DefaultPageSettings.Margins.Left = 0;
            printDocument5.DefaultPageSettings.Margins.Right = 0;
            //下面这个也设成高质量
            e.Graphics.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
            //下面这个设成High
            e.Graphics.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
            e.HasMorePages = false;

            string str = "";
            //下面这个也设成高质量
            e.Graphics.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
            //下面这个设成High
            e.Graphics.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
            e.HasMorePages = false;
            Font titleFont = new Font("黑体", 16, System.Drawing.FontStyle.Bold);//标题字体           
            Font titleFont2 = new Font("黑体", 14, System.Drawing.FontStyle.Bold);//标题字体           
            Font fntTxt = new Font("黑体", 12, System.Drawing.FontStyle.Regular);//正文文字         
            Font fntTxt1 = new Font("黑体", 10, System.Drawing.FontStyle.Regular);//正文文字           
            System.Drawing.Brush brush = new SolidBrush(System.Drawing.Color.Black);//画刷           
            System.Drawing.Pen pen = new System.Drawing.Pen(System.Drawing.Color.Black);//线条颜色         

            try
            {

                printDocument5.DefaultPageSettings.PaperSize.Width = 299;//请注意，这里是单位：（百分之一英寸）
                printDocument5.DefaultPageSettings.PaperSize.Height = 100;//请注意，这里是单位：（百分之一英寸）
                e.PageSettings.PaperSize.Width = 299;//请注意，这里是单位：（百分之一英寸）
                e.PageSettings.PaperSize.Height = 100;//请注意，这里是单位：（百分之一英寸）
                Image barcode = null;
                barcode = GenerateBarCodeBitmap_sub02($"{barcode_sub01}", 220, 50);
                e.Graphics.DrawImage(barcode, new System.Drawing.Point(60, 4));
                e.Graphics.DrawString($"{barcode_sub01}", fntTxt, brush, new System.Drawing.Point(26, 58));

            }
            catch (Exception ee)
            {
                MessageBox.Show(ee.Message);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            printDocument5.PrinterSettings.PrinterName = "Gprinter GP-3100TU";//打印机名字//如果不指定，就是默认打印机
            barcode_sub01 = "1099009900";
            printDocument5.Print();
        }
    }
}
